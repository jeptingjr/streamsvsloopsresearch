package neon.encephalon;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/* Java Streams is definitely an industry buzzword, just because a concept is new and exciting doesn't mean that it
 * should be used everywhere. The point of this code is to test streams vs a traditional for-loop, and find scenarios
 * where one is better than the other.
 *
 * In order to check the performance of each solution, this code was profiled with the..........
 *
 *         Java JFR Profiler IntelliJ Extension: https://plugins.jetbrains.com/plugin/20937-java-jfr-profiler
 *
 * Problem Statement for profiling:
 *      Given a list of integers, order the list, divide it into n even partitions, then find the average of each
 *      partition.
 *
 * The results of the profiling are as follows (ordered from fastest to slowest)
 *      1. loopSolution
 *      2. sequentialStreamSolution
 *      3. parallelStreamSolution
 *      4. hybridSolution
 *
 * Remaining Unknowns:
 *      - The profiler extension requires firefox, does this impact the results? Probably not, it has a 4.5 star rating
 *      - Do I suck at using sequential streams and/or parallel streams? Can these be optimized for this use case?
 *      - Would using docker provide more concise results?
 *
 * My Thoughts:
 *      From my research it seems streams are better suited for when you might find your self needing to write more than
 *      one loop to do work on a collection. This scenario is kind of complex, but will get the point across. Let's say
 *      we have a large collection of large objects. We need to perform a series of expensive operations on each object,
 *      and due to business constraints if a prior operation fails for ANY object, all subsequent operations can be
 *      skipped. Using loops, we would have to define a loop for each operation, and add logic in each loop to
 *      check that an element was processed correctly. Since these operations are expensive, we do not want to run
 *      ANY before we know all prior operations have succeeded or else we will take a gnarly performance hit for no
 *      good reason. With Parallel streams, the run time is so fast, we don't really need logic to protect us from
 *      expensive operations, although doing so would still provide a boost in performance.
 *
 *      In this scenario sequential streams, and parallel streams should run faster as chaining together tasks is
 *      optimized in the streams API. I coded this below, and my hypothesis is half true, with the given scenario here 
 *      is the new speed ranking.
 *
 *      1. Parallel Streams (runValidParallelStreamsUseCase)
 *      ?. Sequential Streams (runValidSequentialStreamsUseCase)
 *      ?. For-Loop (runInvalidLoopUseCase)
 *
 *      Upon further investigation it seems sequential streams and for-loops have inconsistent runtimes, sometimes
 *      the loop wins (by a slim margin), sometimes the stream wins (by a significant amount OR a slim margin). I'm not
 *      sure why, but the answer may be within the inner workings of the profiler, JVM, or maybe something strange
 *      happened with my hardware. I guess for more serious profiling I should use docker. Regardless, more often than
 *      not the for-loop wins.
 *
 * Almost Final Word:
 *      Streams may be better for large datasets where we need to perform multiple expensive operations on said dataset 
 *      in some specific order. For small datasets, where one loop would suffice or multi-threading would break our
 *      logic, traditional java loops are more efficient.
 *
 * A new scenario approaches!
 *      Maybe instead of trying to think of some pseudo-real world scenario for comparing streams vs loops, I should
 *      just compare raw functionality. Problem statement #3 will be simple, we need to sort, filter, map, then reduce
 *      a collection. We know parallel streams will win this race, so I will only compare a sequential stream to a loop.
 *
 *      But alas! The sequential stream is still slower, is there ever a use case for using a sequential stream? Let's
 *      say I'm a junior dev, who struggles with loops, maybe streams would help me?
 *
 *      Wow, even with "poorly" written loops in runJuniorDevLoopForProblemStatement3 we beat streams. This even ran
 *      slightly faster than the singular loop (due to JVM optimizations), not by much though!
 *
 * Another scenario:
 *      I found a youtube video that performed a similar test: https://www.youtube.com/watch?v=EGkU1j6-U3A
 *      Turns out streams get optimized after running once, where loops are so efficient they are barely optimized
 *      further.
 *
 *      I also found this insightful comment:
 *      |Like another comment said, yes the reason behind this is that java prioritizes 2 types of code,
 *      |- Complex Code: Code that is really complex and is a heavy hitter on the system, to reduce slowdowns.
 *      |- Hot Code: Code that is executed a lot of times and reused all the time. Stuff like: Math/String/Lists/Maps
 *      | fall under that.
 *      |
 *      |The reason why the for loop seems to be slower in second and third iterations is mainly,
 *      |Java deems the method fast enough or not important enough to optimize further. Because it is really light
 *      |weight and barely used.
 *      |Streams on the other hand most likely get priority since in older versions it was such a heavy hitter that it
 *      |would have been a dead feature in the end.
 *      |
 *      |But since i really know features I don't like, if you would like to understand why Streams are a bad idea in a
 *      |lot of cases, here are a few things:
 *      |- Streams are state machines, that means you allocate new classes for every single action you perform with
 *      |them.
 *      |- Streams have multiple layers and a lot of reprocessing included. If you want to see how much code you chew
 *      |through just do a semi complex stream (like 2-3 steps) and at the for each just add a break point and look how
 *      |long the callstack is just for 1 entry iterates over. You will be surprised.
 *      |
 *      |In general: Avoid Streams where performance is required/suggested, it is only a convenience tool with a high
 *      |price to pay.
 *      |
 *      |There is a reason why any performance related library that advertises performance barely have any stream
 *      |support to begin with.
 *
 *      The function runYoutubeValidation will be used to validate this. The loop still beats the stream. I've added
 *      snips to this project to showcase the differences in call stacks. The longer the call stack, the more resources
 *      used. Sequential streams really aren't some awesome new thing that should be used everywhere. Really only
 *      parallel streams and traditional loops should be used if we care about performance.
 *
 * ACTUAL FINAL WORD:
 *      I'm at the point where in order for a sequential stream to beat a for-loop I would need to write the stream in
 *      such a way that the JVM optimizes the stream more than it would a loop. This is so far out of the scope of what
 *      developers do in their day to day that finding this super niche situation is rather meaningless.
 *
 *      So here is the verdict:
 *          1. Sequential Streams should be avoided unless their JVM optimization can beat a traditional loop, this is
 *             very very very unlikely
 *          2. For the overwhelming majority of use cases a for-loop will be more performant than a sequential stream
 *          3. Parallel streams are the fastest, however not all use cases are valid for multi-threading, and you
 *             are purchasing speed with extra memory usage.
 */
public class Main {

    public static void main(String[] args) {
        runProblemStatement1(); // Streams perform worse
        runProblemStatement2(); // Only the parallel stream performs better
        runProblemStatement3(); // Even a "poorly written" implementation beats the sequential stream
        runYoutubeValidation(); // I'd have to tinker with this until the JVM optimization for the stream beats the loop
    }

    /*  Problem Statement #1:
     *  Given a list of integers, order the list, divide it into n even partitions, then find the average of each
     *  partition. */
    public static void runProblemStatement1() {
        System.out.println("RUNNING PROBLEM STATEMENT #1");
        runForPerformance();
        runForCorrectness();
    }

    /*  Problem Statement #2:
     *  We must run a number of expensive operations on a list of objects. If each operation is considered a "step"
     *  "step1" mut be performed on all objects before "step2", if any "step1" operations fail, we can cease execution,
     *  due to our imaginary business rules stating if "step1" fails we have no need for "step2" and can throw an
     *  exception. */
    public static void runProblemStatement2() {
        System.out.println("RUNNING PROBLEM STATEMENT #2");
        List<ExampleObject> exampleObjectList = new ArrayList<>();
        for (int x = 0; x < 1000; x++) {
            exampleObjectList.add(new ExampleObject());
        }
        printResults("FOR-LOOP", runInvalidLoopUseCase(exampleObjectList));
        printResults("SEQUENTIAL STREAMS", runValidSequentialStreamsUseCase(exampleObjectList));
        printResults("PARALLEL STREAMS", runValidParallelStreamsUseCase(exampleObjectList));
    }

    /* Problem Statement #3: Sort, filter, map, and reduce a collection */

    public static void runProblemStatement3() {
        System.out.println("RUNNING PROBLEM STATEMENT #3");
        // Prepare 20,000,000 random numbers ranging from 0 to 99,999
        List<Integer> integerList = new ArrayList<>();
        for (int x = 0; x < 20000000; x++) {
            integerList.add((int) Math.floor(Math.random() * 100000));
        }
        System.out.println("STREAM RESULT :: " + runStreamForProblemStatement3(integerList));
        System.out.println("LOOP RESULT :: " + runLoopForProblemStatement3(integerList));
        System.out.println("JUNIOR DEV RESULT :: " + runJuniorDevLoopForProblemStatement3(integerList));
    }

    /* This attempts to validate the results and comments from this video https://www.youtube.com/watch?v=EGkU1j6-U3A */
    public static void runYoutubeValidation() {
        System.out.println("RUNNING YOUTUBE VALIDATION");
        List<Integer> integerList = new ArrayList<>();
        // I'm making this list smaller so we can reach peak JVM optimization faster
        for (int x = 0; x < 2000; x++) {
            integerList.add((int) Math.floor(Math.random() * 100000));
        }
        runMultiLoop(20000, integerList);
        runMultiStream(20000, integerList);
    }

    public static void runMultiLoop(Integer executions, List<Integer> integerList) {
        for(int runCounter = 0; runCounter < executions; runCounter++) {
            runLoopForProblemStatement3(integerList);
        }
    }

    public static void runMultiStream(Integer executions, List<Integer> integerList) {
        for(int runCounter = 0; runCounter < executions; runCounter++) {
            runStreamForProblemStatement3(integerList);
        }
    }

    public static Double runStreamForProblemStatement3(List<Integer> integerList) {
        return integerList.stream()
                .sorted()
                .filter(integer -> integer < 50000)
                .map(integer -> integer * 4.0)
                .reduce(1.0, (accumulator, nextValue) -> (accumulator / 50) - Math.sqrt(nextValue));
    }


    public static Double runLoopForProblemStatement3(List<Integer> integerList) {
        List<Integer> sortedList = QuickSort.instance.sort(integerList);
        double accumulator = 1.0;
        for(Integer integer : sortedList) {
            if(integer < 50000) {
                double newValue = integer * 4.0;
                accumulator = (accumulator / 50) - Math.sqrt(newValue);
            }
        }
        return accumulator;
    }

    public static Double runJuniorDevLoopForProblemStatement3(List<Integer> integerList) {
        // Imagine the quicksort implementation came from a senior developer
        List<Integer> sortedList = QuickSort.instance.sort(integerList);

        List<Integer> filteredList = new ArrayList<>();
        for(Integer integer : sortedList) {
            if(integer < 50000) {
                filteredList.add(integer);
            }
        }

        List<Double> mappedList = new ArrayList<>();
        for(Integer integer : filteredList) {
            mappedList.add(integer * 4.0);
        }

        double accumulator = 1.0;
        for(Double _double : mappedList) {
            accumulator = (accumulator / 50) - Math.sqrt(_double);
        }

        return accumulator;
    }

    public static void runForPerformance() {
        // Prepare 20,000,000 random numbers ranging from 0 to 99,999
        List<Integer> integerList = new ArrayList<>();
        for (int x = 0; x < 20000000; x++) {
            integerList.add((int) Math.floor(Math.random() * 100000));
        }

        // Run each solution
        sequentialStreamSolution(integerList, 5);
        parallelStreamSolution(integerList, 5);
        hybridSolution(integerList, 5);
        loopSolution(integerList, 5);
    }

    // This function runs so fast the profiler has a hard time picking it up, so it may not appear in the results
    public static void runForCorrectness() {
        // This simple list will make the results simple. The average of each partition should be 1, 2, 3, 4, 5
        List<Integer> integerList = Arrays.asList(1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5);

        System.out.println("SEQUENTIAL STREAM :: " + sequentialStreamSolution(integerList, 5));
        System.out.println("PARALLEL STREAM :: " + parallelStreamSolution(integerList, 5));
        System.out.println("HYBRID SOLUTION :: " + hybridSolution(integerList, 5));
        System.out.println("LOOP SOLUTION :: " + loopSolution(integerList, 5));
    }

    public static List<OptionalDouble> sequentialStreamSolution(List<Integer> data, int numberOfPartitions) {
        if (data.size() % numberOfPartitions != 0) {
            throw new RuntimeException("Can't divide data into equal partitions");
        }

        /* Since variables declared outside of lambda functions must be final, we need to use some sort of object
         * counter rather than the traditional integer, remember! a final object can't have a new object reference
         * assigned to it, however we can very much change the internal state of the object itself. */
        final AtomicInteger counter = new AtomicInteger();

        return data.stream()
                .sorted()
                .collect(Collectors.groupingBy((number) -> {
                    return counter.getAndIncrement() / (data.size() / numberOfPartitions);
                }))
                .values().stream()
                .map(partition -> {
                    return partition.stream().mapToInt(integer -> integer).average();
                }).toList();
    }

    public static List<OptionalDouble> parallelStreamSolution(List<Integer> data, int numberOfPartitions) {
        if (data.size() % numberOfPartitions != 0) {
            throw new RuntimeException("Can't divide data into equal partitions");
        }

        final AtomicInteger counter = new AtomicInteger();

        // We can't use a parallel stream prior to sorting, as each parallel thread will be sorted individually
        return data.stream()
                .sorted() // We can't even split the ordered stream into a parallel stream via .sorted().parallel()
                .collect(Collectors.groupingBy((number) -> {
                    return counter.getAndIncrement() / (data.size() / numberOfPartitions);
                }))
                .values().parallelStream()
                .map(partition -> {
                    return partition.parallelStream().mapToInt(integer -> integer).average();
                }).toList();
    }

    public static Map<Integer, Double> hybridSolution(List<Integer> data, int numberOfPartitions) {
        if (data.size() % numberOfPartitions != 0) {
            throw new RuntimeException("Can't divide data into equal partitions");
        }
        final AtomicInteger counter = new AtomicInteger();

        Map<Integer, List<Integer>> partitions = data.stream()
                .sorted()
                .collect(Collectors.groupingBy((number) -> {
                    return counter.getAndIncrement() / (data.size() / numberOfPartitions);
                }));

        Map<Integer, Double> results = new HashMap<>();
        int partitionCounter = 1;
        for (List<Integer> partition : partitions.values()) {
            double sum = partition.parallelStream().reduce(Integer::sum).orElse(0);
            results.put(partitionCounter, sum / partition.size());
            partitionCounter++;
        }
        return results;
    }

    public static Map<Integer, Double> loopSolution(List<Integer> data, int numberOfPartitions) {
        if (data.size() % numberOfPartitions != 0) {
            throw new RuntimeException("Can't divide data into equal partitions");
        }
        /* using a quick sort implementation I stole from the internet to stay true to only using loops */
        List<Integer> sortedData = QuickSort.instance.sort(data);
        double accumulator = 0;
        Map<Integer, Double> averageMap = new HashMap<>();
        int partitionSize = sortedData.size() / numberOfPartitions;
        // We start index at 1 so our integer math for the map keys is cleaner
        for (int index = 1; index <= sortedData.size(); index++) {
            accumulator += sortedData.get(index - 1);
            if (index % partitionSize == 0) {
                averageMap.put(index / partitionSize, accumulator / partitionSize);
                accumulator = 0;
            }
        }
        return averageMap;
    }

    public static void printResults(String solutionName, List<String> results) {
        System.out.println(solutionName + " IS DONE :: Result Count: " + results.size());
    }

    public static List<String> runValidParallelStreamsUseCase(List<ExampleObject> exampleObjectList) {
        return exampleObjectList.parallelStream()
                .map(ExampleObject::expensiveOperation1)
                .map(AnotherExampleObject::expensiveOperation2)
                .collect(Collectors.toList());
    }

    public static List<String> runValidSequentialStreamsUseCase(List<ExampleObject> exampleObjectList) {
        /* Imagine each map function reference has logic that throws an exception if the expensiveOperation fails */
        return exampleObjectList.stream()
                .map(ExampleObject::expensiveOperation1)
                .map(AnotherExampleObject::expensiveOperation2)
                .collect(Collectors.toList());
    }

    public static List<String> runInvalidLoopUseCase(List<ExampleObject> exampleObjectList) {
        List<AnotherExampleObject> resultsFromFirstRoundOfProcessing = new ArrayList<>();
        for(ExampleObject exampleObject : exampleObjectList) {
            resultsFromFirstRoundOfProcessing.add(exampleObject.expensiveOperation1());
            /* Imagine there is code here that throws an exception if expensiveOperation1 ever fails */
        }

        List<String> resultsFromSecondRoundOfProcessing = new ArrayList<>();
        for(AnotherExampleObject anotherExampleObject : resultsFromFirstRoundOfProcessing) {
            resultsFromSecondRoundOfProcessing.add(anotherExampleObject.expensiveOperation2());
            /* Imagine there is code here that throws an exception if expensiveOperation2 ever fails */
        }
        return resultsFromSecondRoundOfProcessing;
    }
}