package neon.encephalon;

import java.util.Arrays;
import java.util.List;

// Using a singleton here since we have state and we have no need to create new instances of this class
// Implementation was taken from: https://www.vogella.com/tutorials/JavaAlgorithmsQuicksort/article.html
public class QuickSort  {

    public static QuickSort instance = init();
    private Integer[] numbers;

    private QuickSort(){}

    private static QuickSort init() {
        if(instance == null) {
            return new QuickSort();
        }
        return instance;
    }

    public List<Integer> sort(List<Integer> values) {
        // Remember, arrays are always faster than ArrayLists because of how they are stored in memory
        this.numbers = values.toArray(new Integer[0]);
        quicksort(0, numbers.length - 1);
        return Arrays.asList(numbers);
    }

    private void quicksort(int low, int high) {
        int i = low, j = high;
        // Get the pivot element from the middle of the list
        int pivot = numbers[low + (high-low)/2];

        // Divide into two lists
        while (i <= j) {
            // If the current value from the left list is smaller than the pivot
            // element then get the next element from the left list
            while (numbers[i] < pivot) {
                i++;
            }
            // If the current value from the right list is larger than the pivot
            // element then get the next element from the right list
            while (numbers[j] > pivot) {
                j--;
            }

            // If we have found a value in the left list which is larger than
            // the pivot element and if we have found a value in the right list
            // which is smaller than the pivot element then we exchange the
            // values.
            // As we are done we can increase i and j
            if (i <= j) {
                exchange(i, j);
                i++;
                j--;
            }
        }
        // Recursion
        if (low < j)
            quicksort(low, j);
        if (i < high)
            quicksort(i, high);
    }

    private void exchange(int i, int j) {
        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }
}